function move(id, x, y, handler)
## move(id, x, y, handler)
## id : kind of object
## x / y
## handler : object's handler
	
	if id == 'c'
		
		auxiliar = get(handler, 'position');
		set(handler, 'position',[ (x - auxiliar(3)/2), (y - auxiliar(4)/2), auxiliar(3), auxiliar(4)]);
		
	else
		set(handler, 'xdata', get(handler, 'xdata') + x - get(handler, 'xdata')(1));
		set(handler, 'ydata', get(handler, 'ydata') + y - get(handler, 'ydata')(1));
	end
	
	
end

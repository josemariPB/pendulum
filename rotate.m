function rotate(id, angle, apx, apy, handler)
## rotate(id, angle, apx, apy, handler)
## id : kind of object
## angle : angle in radians
## ap : anchor point
## handler : object's handler
	
	
	if id == 'c'
		
		auxiliar = get(handler,'Position');
		xaux = auxiliar(1) + auxiliar(3) / 2 - apx;
		yaux = auxiliar(2) + auxiliar(4) / 2 - apy;
		
	else
		xaux = get(handler, 'xdata');
		yaux = get(handler, 'ydata');
		xaux -= apx;
		yaux -= apy;
	end
	
	
	if angle == 0.5 * pi

		x = -yaux;
		y = xaux;

	elseif angle == pi

		x = -xaux;
		y = -yaux;

	elseif angle == 1.5 * pi

		y = -xaux;
		x = yaux;
		
	else
				
		x = (xaux - yaux * tan(angle)) * cos(angle);
		y = yaux / cos(angle) + (xaux - yaux * tan(angle)) * sin(angle);
	
	end
		
	if id == 'c'
	
		x = x + apx - auxiliar(3) / 2;
		y = y + apy - auxiliar(4) / 2;
		set(handler, 'Position', [x y auxiliar(3) auxiliar(4)]);
	
	else
		
		x += apx;
		y += apy;
		set(handler, 'xdata', x(:));
		set(handler, 'ydata', y(:));
	end	
	
		
	
end


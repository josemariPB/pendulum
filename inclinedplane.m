function inclinedplane(planeangle, cof, initialposition, mass)
## inclinedplane(planeangle, cof, initialposition, mass)
##
## Simulate an inclined plane and an object sitting
##    on top of it, that may slide down.
##
##    planeangle: inclination of the plane (degrees)
##    cof: the coefficient of friction ∈ [0, 1]
##    initialposition: position of the object in the plane
##      0.0 = left side
##      0.5 = middle
##      1.0 = right side
##    mass: mass (Kg) of the object
##
##    example: inclinedplane(10, 0.1, 0.9, 1.0)


  ## gravitational acceleration (in m/s/s)

  ## length of the simulation time step (in s)
  clf;
  g = 9.8;

  ## object mass
  if ~exist("mass", "var")
    mass = 1.0;
  end

  ## coefficient of friction (dimensionless)
  if ~exist("cof", "var")
    cof = 0.0;
  end

  ## object position
  if ~exist("initialposition", "var")
    initialposition = 0.5;
  end



  W = g * mass;
  Ff = W * cosd(planeangle) * cof;
  Wx = W * sind(planeangle);
  a  = max((Wx - Ff) / mass, 0);
  planelength = 10;
  
  position_0 = initialposition * planelength / cosd(planeangle);
  movement = {x=@(t) position_0 - a * t * t, @() planeangle, @() planelength};
  condition = @(t) x(t) <= 0.5 || a == 0;
  loop(condition, movement, @drawplane, 0);


end


function drawplane(position, planeangle, planelength)

  cla;
  axis('equal');
  axis('off');
  hold('on');
  
  ## object on the inclined plane
  object.height = 0.8;
  object.width  = 1;
  
  plane.color  = [0.5, 0.5, 0.5];
  object.color = [0.8, 0.8, 0.8];
  plane.angle   = planeangle;
  plane.length  = planelength;
  
  axis([- object.height * sind(planeangle) - 1, plane.length * cosd(planeangle) + 1, 0, plane.length * sind(planeangle) + object.height * cosd(planeangle) + 1]);
  draw('t', [0 0 plane.length 0 plane.length plane.length * tand(planeangle)], plane.color);
  
  object.handler = draw('r', [x=(position - object.width/2) * cosd(planeangle), y=(position - object.width/2) * sind(plane.angle), x + object.width, y + object.height], object.color);
  rotate('r', planeangle * pi / 180, x, y, object.handler);

end

function parabolic(cx, cy, r, v, alfa)
## parabolic(cx, cy, r, v, alfa)
## Simulate the parabolic movement.
##   cx     : projectile's initial position (x)
##   cy     : projectile's initial position (y)
##   r      : projectile's radious
##   v      : velocity
##   alfa   : angle (degrees)
##
## example
##   parabolic(0, 100, 2, 50, 70)

  clf;
  
  g = -9.8;
  vx = v * cosd(alfa);
  vy = v * sind(alfa);
 
  minx = cx - r;
  maxx = cx + r + vx * ((-vy - sqrt((vy) ^ 2 - 4 * 0.5 * g * cy)) / g);
  maxy = cy + r - 0.5 * vy * vy / g;
  
  axis('equal');
  axis('off');
  hold('on');
  axis([minx maxx 0 maxy]);
  
  movement = {@(t) cx + vx * t, y = @(t) cy + vy * t + 0.5 * g * t * t, radious = @() r};
  condition = @(t) y(t) - radious() <= 0;
  loop(condition, movement, @drawparabolic, 0);

end

function drawparabolic(cx, cy, r)
  
  cla;
	
  
  cir = draw('c', [cx cy r], 'r');
  #draw('l', [cx - 2 * r, cy - r, 4 * r, 0, 0.01], 'k');
  draw('l', [axis()(1), 0, axis()(2) - axis()(1), 0, 0.01], 'k');
  	
  k=plot(cx, cy, 'k');
  set(k, 'handlevisibility', 'off');
  

end


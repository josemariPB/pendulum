function handler = draw(id, points, color)
## Draw something
## draw("c", [x1, y1, radious], colour)
## draw("s", [x1, y1, l], colour)
## draw("r", [x1, y1, x2, y2], colour)
## draw("t", [x1, y1, x2, y2, x3, y3], colour)
## draw("a", [x1, y1, x2, y2, p, k, alfa], colour) where 
## p : proporcion de longitud que ocupa la cabeza
## k : se usa para calcular grosor, es la proporcion entre el grosor y la longitud 
## alfa : angulo de la cabeza
##
## draw("l", [x1, y1, lx, ly, k], colour)

	if id == "c"		##circle
	
		handler = rectangle('Position',[(points(1) - points(3)) (points(2) - points(3))  (2 * points(3)) (2 * points(3))],'Curvature',[1,1], 'FaceColor', color);
		
	elseif id == "s"	## square
		
		handler = fill([points(1) points(1) (points(1) + points(3)) (points(1) + points(3))], [points(2) (points(2) + points(3)) (points(2) + points(3)) points(2)], color);
		
	elseif id == "r"	## rectangle
		
		handler = fill([points(1) points(1) points(3) points(3)], [points(2) points(4) points(4) points(2)], color);
		
	elseif id == "t"	## triangle
		
		handler = fill([points(1) points(3) points(5)], [points(2) points(4) points(6)], color);
		
	elseif id == "a"	## arrow
		
		l = sqrt( points(3) ^2 + points(4)^2 );
		if points(3) != 0
			alfa = atan( points(4) / points(3) );
		else
			alfa = pi/2;
		end
		if points(3) < 0
			alfa += pi;
		end
		grosor = points(6) * l;
		
		handler = fill([points(1), points(1), ((1 - points(5)) * l + points(1)), ((1 - points(5)) * l + points(1)), (points(1) + l), ((1 - points(5)) * l + points(1)), ((1 - points(5)) * l + points(1)), points(1)], ...
		 [points(2), (points(2) - 0.5 * grosor), (points(2) - 0.5 * grosor), (points(2) - points(5) * l * tan(points(7)/2)), points(2), (points(2) + points(5) * l * tan(points(7)/2)), (points(2) + 0.5 * grosor), (points(2) + 0.5 * grosor)], color);
		 
		 rotate("a", alfa, points(1), points(2), handler);
		
	elseif id == "l"	## line
		
		l = sqrt( points(3) ^ 2 + points(4) ^ 2 );
		if points(3) != 0
			alfa = atan( points(4) / points(3) );
		else
			alfa = pi/2;
		end
		if points(3) < 0
			alfa += pi;
		end
		grosor = points(5) * l;
		handler = fill([points(1) points(1) (points(1) + l) (points(1) + l)], [(points(2) + grosor/2) (points(2) - grosor/2) (points(2) - grosor/2) (points(2) + grosor/2)], color);
		rotate("l", alfa, points(1), points(2), handler);
		
	end
	
end

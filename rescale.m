function rescale(id, scale, rpx, rpy, handler)
## rescale(id, scale, rpx, rpy, handler)
## id : kind of object
## scale
## rp : reference point
## handler : object's handler
	
	if scale != 1
		if id == 'c'
	
			auxiliar = get(handler, 'position');
			diferencia = [ (auxiliar(1) - rpx) (auxiliar(2) - rpy) ];
			set(handler, 'position', [(rpx + scale * diferencia(1)) (rpy + scale * diferencia(2)) scale * auxiliar(3) scale * auxiliar(4)]);
		
		
		else
			
			auxiliar = get(handler, 'vertices');
			for z = 1:length(auxiliar)
				auxiliar(z,:) = scale * auxiliar(z,:) - [rpx, rpy];
			end
			set(handler, 'vertices', auxiliar);
		end
	end
	
	
end

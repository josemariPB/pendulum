
function pendulum(l, phase, w, A, tag)
## pendulum(l, phase, w, amp, tag)
## Simulate pendulums.
##   l      : length of rope
##   phase  : phase of the movement
##   w      : angular frequency
##   amp    : amplitude
##   tag    : text above the simulation
##
##	 A must be <= l1
## example
##   pendulum({2, 2},{0, 0},{sqrt(9.8/2), sqrt(1.6/2)},{0.7, 0.7},{'Tierra', 'Luna'})
  clf;

  for i = 1:length(l)
	
	expresions{end + 1} = {x=@(t) A{i} * cos(w{i} * t + phase{i}), @(t) sqrt(l{i} * l{i} - A{i} * cos(w{i} * t + phase{i}) * A{i} * cos(w{i} * t + phase{i})),...
	 @(t) -A{i} * w{i} * sin(w{i} * t + phase{i}), @(t) -x(t) * w{i} * w{i}, @() tag{i}};
	
  end											# functions
  loop(@() 0, expresions, @drawpendulum, 1);

end

function drawpendulum(cx, cy, v, a, tag)															#	draw the pendulum
  cla; 
  wallcolor = [0.1, 0.1, 0.1];
  diskcolor = [0.5, 0.5, 0.5];
  ropecolor = [0.8, 0.8, 0.8];
  cy = -cy;
  scale = 1;
## set axis
	axis('equal');
	axis('off');
	hold('on');
	
	l = sqrt(cx * cx + cy * cy);
	r = l / 10;
	
    axis([- l - r , l + r ,- l - r , r ]);


## draw
	text(0, 0.3  * l, tag,"fontsize",15,"horizontalalignment","center");
    rec = draw('r', [-0.5 * l, 0,  0.5 * l,  0.1 * l], wallcolor);																		#	draw the base

	cir = draw('c', [cx , cy , r ], diskcolor);																		#	draw the circle

	lin = draw('l', [0, 0, cx  - r  * sin(atan((cx )  / -(cy ) )), cy  + r *cos(atan((cx ) / -(cy ))), 0.01], ropecolor);		#	draw the line
	
	vectorV = draw('a', [cx, cy, v * scale, 0 , 0.2, 0.01, pi/3], 'r');
	textoV = text(cx + v * scale , cy + 0.2 * l,  'v',"fontsize",25,"horizontalalignment","center", "fontweight", "bold", "fontangle", "italic");
	VectorA = draw('a', [cx, cy, a * scale, 0 , 0.2, 0.01, pi/3], 'g');
	textoV = text(cx + a * scale , cy + 0.2 * l,  'a',"fontsize",25,"horizontalalignment","center", "fontweight", "bold", "fontangle", "italic");

end


function loop(condition, movement, drawing, stop)
#	Function to Simulate
#	
#	condition : Condition to Stop
#	movement : function of movement
#	drawing : function of drawing
#	stop : do you want a stop button?
#
#	Example:
#		movement = {x=@(t) position_0 - a * t * t, @() planeangle, @() planelength};
#		condition = @(t) x(t) <= 0.5 || a == 0;
#  		loop(condition, movement, @drawplane, 0);
#
  t = 0;
  timebin = 0.01;
  button = 0;
  if stop 
	b1 = uicontrol ("style","pushbutton","string", "Stop", "position",[10 10 150 40], 'callback',@(b1) set(b1, 'value', 1));
  end
  do
																							## start measuring elapsed time
    tic;
    t += timebin;
    if stop
		button = get(b1, 'value');
	end
	if(~iscell(movement{1}))
	
		newpos = {};
		for i = 1:length(movement)
		
			newpos{end + 1} = movement{i}(t);
		
		end
		feval(drawing, newpos{:});
		
	else
	
		for j = 1:length(movement)
			subplot(1, length(movement), j);
			newpos = {};
			for i = 1:length(movement{j})
		
				newpos{end + 1} = movement{j}{i}(t);
		
			end
			feval(drawing, newpos{:});
	
		end
	end
    
																							## computation and rendering is done, measure real time again
    elapsedtime = toc;
    if timebin - elapsedtime < 0
      timebin = elapsedtime;
    end
    pause(timebin - elapsedtime);															## frame's control
  until condition(t) || (stop && button) ;
	

end


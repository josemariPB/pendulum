function reassign(x1, y1, x2, y2, p, k, alfa, handler)
## reassign(x1, y1, x2, y2, p, k, alfa, handler)
## change where the vector points
		
		l = sqrt( (x2)^2 + (y2)^2 );
		if x2 != 0
			angle = atan( ( y2 ) / (x2) );
		else
			angle = pi/2;
		end
		if x2 < 0
			angle += pi;
		end
		
		grosor = k * l;
		set(handler, 'xdata', [x1, x1, ((1 - p) * l + x1), ((1 - p) * l + x1), (l + x1), ((1 - p) * l + x1), ((1 - p) * l + x1), x1]);
		set(handler, 'ydata', [y1, (y1 - 0.5 * grosor), (y1 - 0.5 * grosor), (y1 - p * l * tan(alfa/2)), y1, (y1 + p * l * tan(alfa / 2)), (y1 + 0.5 * grosor), (y1 + 0.5 * grosor)]);
		 
		rotate("a", angle, x1, y1, handler);
		
		
end
